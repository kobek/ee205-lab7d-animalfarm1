///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    addCats.h
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date 01_Mar_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <stdbool.h>
#include "catDatabase.h"

extern int addCat(const char name[], 
      const enum genderType gender,
      const enum breedType breed, 
      const bool isFixed, 
      const float weight, 
      const enum color collarColor1, 
      const enum color collarCollar2, 
      const unsigned long long license);
