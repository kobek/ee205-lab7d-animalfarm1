///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file    config.h
/// @version 1.0 - Initial version
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date 01_Mar_2022
///////////////////////////////////////////////////////////////////////////////
//

#define DATABASE_FILE_NAME    "catDatabase.c"
#define ADD_CATS_FILE_NAME    "addCats.c"
#define REPORT_CATS_FILE_NAME "repoprtCats.c"
#define UPDATE_CATS_FILE_NAME "updateCats.c"
#define DELETE_CATS_FILE_NAME "deleteCats.c"
#define MAIN_FILE_NAME        "main.c"
